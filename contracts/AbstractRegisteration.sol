pragma solidity ^0.4.2;

contract RegisterationAuthority {
    
    function isRegisteredUser(bytes32 id) public view returns(bool);

    function isRegisteredAddress(address publicKey) public view returns(bool);

    function registerUser(bytes32 name, bytes32 homeAddress,
    	bytes32 id, address publicKey) public returns(bool);
}
