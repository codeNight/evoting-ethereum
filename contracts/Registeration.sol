pragma solidity ^0.4.2;

import "./AbstractRegisteration.sol";

contract Registeration is RegisterationAuthority{
    struct User{
        bytes32 name;
        bytes32 homeAddress;
        bool isUser;
        bool canVote;
    }

    mapping(bytes32 => User) private registeredUsers;
    bytes32[] private registeredUsersIds;

    mapping(address => bytes32) publicKeyToId;
    address[] private registeredAddresses;
    
    function isRegisteredUser(bytes32 id) public view returns(bool) {
        return registeredUsers[id].isUser;
    }

    function isRegisteredAddress(address publicKey) public view returns(bool) {
        return isRegisteredUser(publicKeyToId[publicKey]);
    }

    function registerUser(bytes32 name, bytes32 homeAddress, bytes32 id, address publicKey)
        public
        returns(bool)
    {
        if(isRegisteredUser(id))
            revert("Unable to register user, user has already registered");

        registeredUsers[id].name = name;
        registeredUsers[id].homeAddress = homeAddress;

        registeredUsers[id].isUser = true;
        registeredUsers[id].canVote = true;

        publicKeyToId[publicKey] = id;

        registeredUsersIds.push(id);
        registeredAddresses.push(publicKey);

        return true;
    }
}
