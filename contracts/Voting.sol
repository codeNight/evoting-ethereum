pragma solidity ^0.4.2;

import "./AbstractRegisteration.sol";

contract Voting {
    struct Candidate{
        bytes32 name;
        bytes32 homeAddress;

        bool isCandidate;
        bytes32[] votesId;
    }

    mapping(bytes32 => Candidate) private registeredCandidates;
    bytes32[] private registeredCandidatesId;

    mapping(address => bool) private voted;

    RegisterationAuthority private registerationAuthority;

    constructor (address registeraionAddress) {
        registerationAuthority = RegisterationAuthority(registeraionAddress);
    }

    function registerCandidate(bytes32 name, bytes32 homeAddress, bytes32 nationalId) 
        public
        returns(bool)
    {
        if(registeredCandidates[nationalId].isCandidate) {
            revert('Unable to register candidate, candidate is already registered');
        }

        registeredCandidates[nationalId].name = name;
        registeredCandidates[nationalId].homeAddress = homeAddress;

        registeredCandidates[nationalId].isCandidate = true;

        registeredCandidatesId.push(nationalId);
    }

    function voteForCandidate(bytes32 candidate, bytes32 voteId,
            address voterPublicKey, uint8 v, bytes32 s, bytes32 r)
        public
        returns(bool)
    {
        if(!isValidSignature(voterPublicKey, candidate, v, s, r)
            || !registerationAuthority.isRegisteredAddress(voterPublicKey)
            || voted[voterPublicKey]) {
            revert('User do not have the right to vote');
        }

        if(!registeredCandidates[candidate].isCandidate) {
            revert('User tried to vote for an unknown candidate');
        }

        registeredCandidates[candidate].votesId.push(voteId);
        voted[voterPublicKey] = true;

        return true;
    }

    function getCandidatesIds() public view returns(bytes32[]) {
        return registeredCandidatesId;
    }

    function getElectionResultsForCandidate(bytes32 candidateId) 
        external
        view
        returns(bytes32[])
    {
        if(!registeredCandidates[candidateId].isCandidate) {
            revert('Not a valid candidate');
        }

        return registeredCandidates[candidateId].votesId;
    }

    function getCandidateInfo(bytes32 nationalId)
        public
        view
        returns(bytes32, bytes32, bytes32) // id, name , homeAddress
    {
        if(!registeredCandidates[nationalId].isCandidate) {
            revert('Not a valid candidate');
        }

        return (nationalId,
            registeredCandidates[nationalId].name,
            registeredCandidates[nationalId].homeAddress);
    }

    function isValidSignature(address potentialSender, 
            bytes32 message, uint8 v, bytes32 s, bytes32 r)
        private
        view
        returns(bool)
    {
        return ecrecover(message, v, r, s) == potentialSender;
    }
}
