
/*
 * This holds all the functions for the app
 */
window.App = {
  // called when web3 is set up
  start: function() {
    // setting up contract providers and transaction defaults for ALL contract instances
    console.log(window.web3.version);
    $.get('/candidates/all', {}, function(response){
      console.log(response.ids);

      for(i = 0 ; i < response.ids.length ; i++) {
        $('#candidates').append('<input type="radio" name="candidate-choice" value="' +  response.ids[i] + '"/>' + response.ids[i] + '</input><br>');
      }
    });
  },

  vote: function() {
    var publicKey = document.getElementById('pub').value;
    var privateKey = document.getElementById('priv').value;
    var choice = document.querySelector('input[name="candidate-choice"]:checked').value;



    console.log("Vote for : " + publicKey + " " + privateKey + " " + choice);
    console.log(web3.eth.accounts);
  }
}

// When the page loads, we create a web3 instance and set a provider. We then set up the app
window.addEventListener("load", function() {
  // Is there an injected web3 instance?
  if (typeof web3 !== "undefined") {
    console.warn("Using web3 detected from external source like Metamask")
    // If there is a web3 instance(in Mist/Metamask), then we use its provider to create our web3object
    window.web3 = new Web3(web3.currentProvider)
  } else {
    console.warn("No web3 detected. Falling back to http://localhost:9545. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for deployment. More info here: http://truffleframework.com/tutorials/truffle-and-metamask")
    // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
    window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:9545"))
  }
  // initializing the App
  window.App.start()
})
