const express = require('express');
const app = express();
const port = 3000 || process.env.PORT;
const Web3 = require('web3');
const registeration_connect = require('./connection/registeration_connection.js')
const voting_connect = require('./connection/voting_connection.js')

const bodyParser = require('body-parser');

var web3;
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// parse application/json
app.use(bodyParser.json());

app.use('/', express.static('public_static'));

// app.get('/getAccounts', (req, res) => {
//   // console.log("**** GET /getAccounts ****");
//   // truffle_connect.start(function (answer) {
//   //   res.send(answer);
//   // })
// });

app.post('/registerUser', (req, res) => {
    console.log("**** POST /registerUser ****");

    console.log(req.body);

    var defaultAccount;
    var newAccount = web3.eth.accounts.create();

    var pub = newAccount.address;
    var priv = newAccount.privateKey;
    
    web3.eth.personal.getAccounts().then(function(accounts){
    registeration_connect.setModuleDefaults({from: accounts[0], gas: 6721975});
        registeration_connect.registerUser(req.body.name, req.body.homeAddress, req.body.nationalId, newAccount.address)
            .then(function(result) {
                res.status(201).json({publicKey: pub, privateKey: priv, transactionReceipt: result});
            }).catch(function(error){
                console.log(error);
                res.status(208).json({error: error.toString()});
            });
    });
});

app.get('/candidates/register', (req, res) => {
    console.log('**** GET /registerCandidate ********');
    res.sendFile('/home/ali/workspace/GraduationProject/EvotingService/public_static/registerCandidate.html');
});

app.get('/voting/vote', (req, res) => {
    console.log('**** GET /Voting.html ********');
    res.sendFile('/home/ali/workspace/GraduationProject/EvotingService/public_static/voting.html');
});

app.post('/candidates/register', (req, res) => {
    console.log("**** POST /registerCandidate ****");
    
    web3.eth.personal.getAccounts().then(function(accounts){
        voting_connect.setModuleDefaults({from: accounts[0], gas: 6721975});
        console.log('requuest dump' + req.body.name + ' ' + req.body.homeAddress + ' ' + req.body.nationalId);
        
        voting_connect.registerCandidate(req.body.name, req.body.homeAddress, req.body.nationalId)
            .then(function(result) {
                console.log(result);
                res.status(200).json({status: 'Candidate registered successfully'});
            }).catch(function(error){
                console.log(error);
                res.status(208).json({error: error.toString()});
            });
    });
});

app.get('/candidates', (req, res) => {
    res.sendFile('/home/ali/workspace/GraduationProject/EvotingService/public_static/candidates_view.html');
})

app.get('/candidates/all', (req, res) => {
    web3.eth.personal.getAccounts().then(function(accounts){
        voting_connect.setModuleDefaults({from: accounts[0], gas: 6721975});
        
        voting_connect.getRegisteredCandidatesIds()
            .then(function(result) {
                var candidates = result.map((val) => {
                    return new String(web3.utils.hexToAscii(val));
                });
                console.log(candidates);
                res.status(200).json({ids: candidates});
            }).catch(function(error){
                console.log(error);
                res.status(208).json({error: error.toString()});
            });
    });
});

app.post('/candidates/describe', (req, res) => {
    web3.eth.personal.getAccounts().then(function(accounts){
        voting_connect.setModuleDefaults({from: accounts[0], gas: 6721975});
        console.log("payload: ");
        console.log(req.body.ids[0]);

        var promises = req.body.ids.map((val) => {
            return voting_connect.getCandidateInfo(val);
        })

        Promise.all(promises)
            .then(function(result){
                console.log(result);
                result = result.map((val) => {
                    console.log("VAL + " + val);
                    return val.map(web3.utils.hexToAscii);
                });
                res.status(200).json({info: result});
            })
            .catch(function(error){
                console.log(error);
                res.status(208).json({error: error.toString()});
            });
    });
});

app.listen(port, () => {
  if (typeof this.web3 !== 'undefined') {
    console.warn("Using web3 detected from external source. If you find that your accounts don't appear or you have 0 MetaCoin, ensure you've configured that source properly. If using MetaMask, see the following link. Feel free to delete this warning. :) http://truffleframework.com/tutorials/truffle-and-metamask")
    // Use Mist/MetaMask's provider
    web3 = new Web3(web3.currentProvider);
    registeration_connect.web3 = web3;
    voting_connect.web3 = web3;
  } else {
    console.warn("No web3 detected. Falling back to http://127.0.0.1:9545. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for development. More info here: http://truffleframework.com/tutorials/truffle-and-metamask");
    // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
    web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:9545"));
    web3.eth.personal.getAccounts().then(console.log);

    registeration_connect.web3 = web3;
    voting_connect.web3 = web3;
  }
  console.log("Express Listening at http://localhost:" + port);
});