const Registeration = artifacts.require("Registeration");



contract('Test Registering a new User', function(accounts) {

    const name = web3.utils.fromAscii("fullName");
    const homeAddress = web3.utils.fromAscii("fullAddress");
    const nationalId = web3.utils.fromAscii("id-number");
    const account = web3.eth.accounts.create();
    
    it("should register a user successfully", function() {
        return Registeration.deployed().then(function(instance){
            return instance.registerUser(name, homeAddress, nationalId, account.address);
        });
    });

    it("should recoginze the nationalId of the previously registered user", function(){
        return Registeration.deployed().then(function(instance){
            return instance.isRegisteredUser(nationalId);
        }).then(function(result){
            return assert.isTrue(result);
        });
    });

    it("should recoginze the public key of the previously registered user", function(){
        return Registeration.deployed().then(function(instance){
            return instance.isRegisteredAddress(account.address);
        }).then(assert.isTrue);
    });

    it("shouldn't recoginze the public key of an unknown user", function(){
        let newAccount = web3.eth.accounts.create();
        return Registeration.deployed().then(function(instance){
            return instance.isRegisteredAddress(newAccount.address);
        }).then(assert.isFalse);
    });

    it("shouldn't recoginze the nationalId of an unknown user", function(){
        let newNationalId = web3.utils.fromAscii("SomeOtherUnknownId");
        return Registeration.deployed().then(function(instance){
            return instance.isRegisteredUser(newNationalId);
        }).then(assert.isFalse);
    });
});
