
const contract = require('truffle-contract');

const registeration_artifact = require('../build/contracts/Registeration.json');
var Registeration = contract(registeration_artifact);
// 
module.exports = {
	setModuleDefaults: function(defaults) {
		Registeration.defaults(defaults);
	},
	registerUser: function(name, homeAddress, nationalId, address) {
		// console.log(this.web3)
		Registeration.setProvider(this.web3.currentProvider);

		if (typeof Registeration.currentProvider.sendAsync !== "function") {
	        Registeration.currentProvider.sendAsync = function() {
	            return Registeration.currentProvider.send.apply(
	                Registeration.currentProvider, arguments
	            );
	        };
	    }

		return Registeration.deployed().then(function(instance){
			return instance.registerUser(name, homeAddress, nationalId, address);
		});
	},
	isRegisteredUser: function(nationalId){
		Registeration.setProvider(this.web3.currentProvider);
	    if (typeof Registeration.currentProvider.sendAsync !== "function") {
	        Registeration.currentProvider.sendAsync = function() {
	            return Registeration.currentProvider.send.apply(
	                Registeration.currentProvider, arguments
	            );
	        };
	    }

		return Registeration.deployed().then(function(instance){
			return instance.isRegisteredUser(nationalId);
		});
	},
	isRegisteredAddress: function(address){
		Registeration.setProvider(this.web3.currentProvider);
	    if (typeof Registeration.currentProvider.sendAsync !== "function") {
	        Registeration.currentProvider.sendAsync = function() {
	            return Registeration.currentProvider.send.apply(
	                Registeration.currentProvider, arguments
	            );
	        };
	    }
    	return Registeration.deployed().then(function(instance){
    		return instance.isRegisteredAddress(address);
    	});
	},
 }