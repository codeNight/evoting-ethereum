
const contract = require('truffle-contract');

const voting_artifact = require('../build/contracts/Voting.json');
var Voting = contract(voting_artifact);
// 
module.exports = {
    setModuleDefaults: function(defaults) {
        Voting.defaults(defaults);

        Voting.setProvider(this.web3.currentProvider);

        if (typeof Voting.currentProvider.sendAsync !== "function") {
            Voting.currentProvider.sendAsync = function() {
                return Voting.currentProvider.send.apply(
                    Voting.currentProvider, arguments
                );
            };
        }
    },

    registerCandidate: function(name, homeAddress, nationalId, address) {
        return Voting.deployed().then(function(instance){
            return instance.registerCandidate(module.exports.toHex(name),
                module.exports.toHex(homeAddress), module.exports.toHex(nationalId));
        });
    },

    getRegisteredCandidatesIds: function() {
        return Voting.deployed().then(function(instance){
            return instance.getCandidatesIds();
        });
    },

    getCandidateInfo: function(nationalId) {
        return Voting.deployed().then(function(instance) {
            return instance.getCandidateInfo(nationalId);
        });
    },

    getElectionResultsForCandidate: function(nationalId) {
        return Voting.deployed.then(function(instance){
            return instance.getElectionResultsForCandidate(nationalId);
        });
    },

    voteForCandidate: function(candidate, voteId, voterPublicKey, v, s, r) {
        return Voting.deployed.then(function(instance){
            return instance.voteForCandidate(candidate, voteId, voterPublicKey, v, s ,r);
        })
    },
    toHex: function(string) {
        console.log("2 voting connection :: web3 version " + this.web3);
        return this.web3.utils.asciiToHex(string);
     }
 }

